package com.example.pmdm_t3a9_furio_andreu.activities;

import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.os.Bundle;

import com.example.pmdm_t3a9_furio_andreu.R;
import com.example.pmdm_t3a9_furio_andreu.pojo.Cliente;

import java.lang.reflect.Method;

public class Dashboard extends AppCompatActivity {

    private TextView txtGreeting;
    private Cliente cliente;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashborad);

        txtGreeting = findViewById(R.id.txtGreeting);
        cliente = (Cliente) getIntent().getSerializableExtra("client");
        txtGreeting.setText(getString(R.string.greeting) + " " + cliente.getNombre());
        toolbar = findViewById(R.id.dashboardToolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setBackgroundDrawable(ContextCompat.getDrawable(this, R.color.background));
    }

    public void changePass(View v) {
        Intent intent = new Intent(Dashboard.this, PasswordChange.class);
        intent.putExtra("client", cliente);
        startActivity(intent);
    }

    public void transfer(View v) {
        Intent intent = new Intent(Dashboard.this, Transfer.class);
        intent.putExtra("client", cliente);
        startActivity(intent);
    }

    public void globalPos(View v) {
        Intent intent = new Intent(Dashboard.this, GlobalPosition.class);
        intent.putExtra("client", cliente);
        startActivity(intent);
    }

    public void exit(View v) {
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboardmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.menu_global_position:
                intent = new Intent(Dashboard.this, GlobalPosition.class);
                intent.putExtra("client", cliente);
                startActivity(intent);
                return true;

            case R.id.menu_stats:

                return true;

            case R.id.menu_transfer:
                intent = new Intent(Dashboard.this, Transfer.class);
                intent.putExtra("client", cliente);
                startActivity(intent);
                return true;

            case R.id.menu_password_change:
                intent = new Intent(Dashboard.this, PasswordChange.class);
                intent.putExtra("client", cliente);
                startActivity(intent);
                return true;

            case R.id.menu_promotions:

                return true;

            case R.id.menu_atms:

                return true;

            case R.id.action_settings:

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }
}