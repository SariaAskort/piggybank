package com.example.pmdm_t3a9_furio_andreu.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.motion.widget.MotionLayout;
import androidx.constraintlayout.widget.Guideline;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.pmdm_t3a9_furio_andreu.R;
import com.example.pmdm_t3a9_furio_andreu.bd.MiBancoOperacional;
import com.example.pmdm_t3a9_furio_andreu.fragments.AccountListener;
import com.example.pmdm_t3a9_furio_andreu.fragments.FragmentCuentas;
import com.example.pmdm_t3a9_furio_andreu.fragments.FragmentMovimientos;
import com.example.pmdm_t3a9_furio_andreu.pojo.Cliente;
import com.example.pmdm_t3a9_furio_andreu.pojo.Cuenta;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class GlobalPosition extends AppCompatActivity implements AccountListener, BottomNavigationView.OnNavigationItemSelectedListener {

    private ListView accountList;
    private ListView movementsList;
    private Guideline midGuidelineGlobalPos;
    private Guideline guidelineGlobalPosSubMid;
    private Button btnReturn;
    private TextView txtMovements;
    private MiBancoOperacional mbo;
    private BottomNavigationView movementsType = null;
    private Cuenta cuentaSeleccionada = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_position);

        mbo = MiBancoOperacional.getInstance(getApplicationContext());

        Cliente cliente = (Cliente) getIntent().getSerializableExtra("client");

        FragmentCuentas cuentas = (FragmentCuentas) getSupportFragmentManager().findFragmentById(R.id.frgAccounts);
        Bundle bundle = new Bundle();
        bundle.putSerializable("cuentas", cliente.getListaCuentas());
        cuentas.setArguments(bundle);
        cuentas.setOnAccountSelected(this);

        if (findViewById(R.id.movementsContainer) != null) {
            movementsType = findViewById(R.id.movementsType);
            movementsType.setOnNavigationItemSelectedListener(this);
            movementsType.setItemIconTintList(null);
        }
    }

    @Override
    public void onAccountSelected(Cuenta cuenta) {
        boolean hayMovimientos = findViewById(R.id.movementsContainer) != null;
        if (hayMovimientos) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.movementsContainer, FragmentMovimientos.newInstance(mbo.getMovimientos(cuenta)))
                    .commit();
            MotionLayout globalposMotion = findViewById(R.id.globalpos_motion);
            globalposMotion.transitionToEnd();
            movementsType.setSelectedItemId(R.id.menu_all_types);

        } else {
            Intent intent = new Intent(this, Movements.class);
            intent.putExtra("cuenta", cuenta);
            startActivity(intent);
        }
        cuentaSeleccionada = cuenta;
    }

    public void exit(View v) {
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (cuentaSeleccionada != null) {
            FragmentMovimientos f = null;
            switch (item.getItemId()) {
                case R.id.menu_all_types:
                    f = FragmentMovimientos.newInstance(mbo.getMovimientos(cuentaSeleccionada));
                    break;

                case R.id.menu_type_zero:
                    f = FragmentMovimientos.newInstance(mbo.getMovimientosTipo(cuentaSeleccionada, 0));
                    break;

                case R.id.menu_type_one:
                    f = FragmentMovimientos.newInstance(mbo.getMovimientosTipo(cuentaSeleccionada, 1));
                    break;

                case R.id.menu_type_two:
                    f = FragmentMovimientos.newInstance(mbo.getMovimientosTipo(cuentaSeleccionada, 2));
                    break;
            }
            if (f != null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.movementsContainer, f)
                        .commit();
            }
        }

        return true;
    }
}