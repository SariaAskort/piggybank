package com.example.pmdm_t3a9_furio_andreu.activities;

import android.content.Intent;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.pmdm_t3a9_furio_andreu.R;
import com.example.pmdm_t3a9_furio_andreu.bd.MiBancoOperacional;
import com.example.pmdm_t3a9_furio_andreu.pojo.Cliente;
import com.example.pmdm_t3a9_furio_andreu.pojo.Cuenta;

public class LoginScreen extends AppCompatActivity {

    private EditText edUser;
    private EditText edPass;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        edUser = findViewById(R.id.edUser);
        edPass = findViewById(R.id.edPass);
        btnLogin = findViewById(R.id.btnLogin);

        if (edUser.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }



    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            if (edUser.hasFocus()) {
                if (!edUser.getText().toString().equals("")){
                    edUser.clearFocus();
                    edPass.requestFocus();
                    return true;
                }
                return false;
            } else if (edPass.hasFocus()) {
                if (!edPass.getText().toString().equals("")) {
                    edPass.clearFocus();
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    btnLogin.performClick();
                    return true;
                }
                return false;
            }
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            if (edUser.hasFocus()) {
                if (!edUser.getText().toString().equals("")){
                    edUser.clearFocus();
                    edPass.requestFocus();
                    return true;
                }
                return false;
            } else if (edPass.hasFocus()) {
                if (!edPass.getText().toString().equals("")) {
                    edPass.clearFocus();
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    btnLogin.performClick();
                    return true;
                }
                return false;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void onClick(View v) {
        if (!edUser.getText().toString().isEmpty() && !edPass.getText().toString().isEmpty()) {
            Cliente cliente = new Cliente();
            cliente.setNif(edUser.getText().toString());
            cliente.setClaveSeguridad(edPass.getText().toString());
            MiBancoOperacional mbo = MiBancoOperacional.getInstance(this);
            Cliente resultado = mbo.login(cliente);
            if (resultado != null) {
                resultado = fillCliente(resultado, mbo);
                Intent intent = new Intent(LoginScreen.this, Dashboard.class);
                intent.putExtra("client", resultado);
                startActivity(intent);
            } else Toast.makeText(getApplicationContext(), getString(R.string.incorrectUser), Toast.LENGTH_LONG).show();
        } else Toast.makeText(getApplicationContext(), getString(R.string.noUserPass), Toast.LENGTH_LONG).show();
    }

    public Cliente fillCliente(Cliente cliente, MiBancoOperacional mbo) {
        cliente.setListaCuentas(mbo.getCuentas(cliente));
        for (Cuenta cuenta :
                cliente.getListaCuentas()) {
            cuenta.setListaMovimientos(mbo.getMovimientos(cuenta));
        }
        return cliente;
    }
}