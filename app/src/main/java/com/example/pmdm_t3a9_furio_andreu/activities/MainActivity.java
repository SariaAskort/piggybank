package com.example.pmdm_t3a9_furio_andreu.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.daasuu.ei.Ease;
import com.daasuu.ei.EasingInterpolator;
import com.example.pmdm_t3a9_furio_andreu.R;

public class MainActivity extends AppCompatActivity {

    private ImageView piggy;
    private TextView welcomeText;
    private ProgressBar progressWelcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        piggy = findViewById(R.id.piggy);
        welcomeText = findViewById(R.id.welcomeText);
        progressWelcome = findViewById(R.id.progressWelcome);

        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(1000);

        AnimationSet animation = new AnimationSet(false);
        animation.addAnimation(fadeIn);
        piggy.setAnimation(animation);
        welcomeText.setAnimation(animation);

        AnimationSet animation2 = new AnimationSet(false);
        animation2.addAnimation(fadeIn);
        animation2.setStartOffset(1500);
        progressWelcome.setAnimation(animation2);

        ObjectAnimator bounceAnimation = ObjectAnimator.ofFloat(piggy, "translationY", 0, 75, 0);
        bounceAnimation.setInterpolator(new EasingInterpolator(Ease.ELASTIC_IN_OUT));
        bounceAnimation.setStartDelay(1500);
        bounceAnimation.setDuration(1500);
        bounceAnimation.setRepeatCount(2);
        bounceAnimation.start();


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(MainActivity.this, LoginScreen.class);
                startActivity(intent);
            }
        }, 4000);
    }
}