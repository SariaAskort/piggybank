package com.example.pmdm_t3a9_furio_andreu.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.pmdm_t3a9_furio_andreu.R;
import com.example.pmdm_t3a9_furio_andreu.bd.MiBancoOperacional;
import com.example.pmdm_t3a9_furio_andreu.fragments.FragmentMovimientos;
import com.example.pmdm_t3a9_furio_andreu.pojo.Cuenta;
import com.example.pmdm_t3a9_furio_andreu.pojo.Movimiento;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class Movements extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    private BottomNavigationView movementsType;
    private MiBancoOperacional mbo;
    private Cuenta cuenta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movements);

        movementsType = findViewById(R.id.movementsType);
        movementsType.setOnNavigationItemSelectedListener(this);
        movementsType.setItemIconTintList(null);

        mbo = MiBancoOperacional.getInstance(getApplicationContext());

        cuenta = (Cuenta) getIntent().getSerializableExtra("cuenta");

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.movementsContainer, FragmentMovimientos.newInstance(mbo.getMovimientos(cuenta)))
                .commit();
    }

    public void exit(View v) {
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        FragmentMovimientos f = null;
        switch (item.getItemId()) {
            case R.id.menu_all_types:
                f = FragmentMovimientos.newInstance(mbo.getMovimientos(cuenta));
                break;

            case R.id.menu_type_zero:
                f = FragmentMovimientos.newInstance(mbo.getMovimientosTipo(cuenta, 0));
                break;

            case R.id.menu_type_one:
                f = FragmentMovimientos.newInstance(mbo.getMovimientosTipo(cuenta, 1));
                break;

            case R.id.menu_type_two:
                f = FragmentMovimientos.newInstance(mbo.getMovimientosTipo(cuenta, 2));
                break;
        }
        if (f != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.movementsContainer, f)
                    .commit();
        }

        return true;
    }
}