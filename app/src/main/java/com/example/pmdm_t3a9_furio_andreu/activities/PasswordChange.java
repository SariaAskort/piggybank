package com.example.pmdm_t3a9_furio_andreu.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pmdm_t3a9_furio_andreu.R;
import com.example.pmdm_t3a9_furio_andreu.bd.MiBancoOperacional;
import com.example.pmdm_t3a9_furio_andreu.pojo.Cliente;

public class PasswordChange extends AppCompatActivity {

    private EditText edOldPass;
    private EditText edNewPass;
    private EditText edRepPass;
    private Button btnChange;
    private Cliente cliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change);

        edOldPass = findViewById(R.id.edOldPass);
        edNewPass = findViewById(R.id.edNewPass);
        edRepPass = findViewById(R.id.edRepPass);
        btnChange = findViewById(R.id.btnChange);

        if (edOldPass.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }

        cliente = (Cliente) getIntent().getSerializableExtra("client");
    }

    public void onClick(View v) {
        String oldPass = edOldPass.getText().toString();
        final String newPass = edNewPass.getText().toString();
        String repPass = edRepPass.getText().toString();

        if (!oldPass.isEmpty() && !newPass.isEmpty() && !repPass.isEmpty()) {
            if (cliente.getClaveSeguridad().equals(oldPass)) {
                if (!oldPass.equals(newPass)) {
                    if (newPass.equals(repPass)) {
                        new AlertDialog.Builder(this)
                                .setTitle(getString(R.string.confirmation))
                                .setMessage(getString(R.string.passConfirmation))
                                .setIcon(R.mipmap.attention)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        MiBancoOperacional mbo = MiBancoOperacional.getInstance(getApplicationContext());
                                        cliente.setClaveSeguridad(newPass);
                                        mbo.changePassword(cliente);
                                        Intent intent = new Intent(PasswordChange.this, Dashboard.class);
                                        intent.putExtra("client", cliente);
                                        startActivity(intent);
                                    }
                                })
                                .setNegativeButton(android.R.string.no, null).show();
                    } else
                        Toast.makeText(getApplicationContext(), getString(R.string.passDontMatch), Toast.LENGTH_LONG).show();
                } else
                    Toast.makeText(getApplicationContext(), getString(R.string.samePassword), Toast.LENGTH_LONG).show();
            } else
                Toast.makeText(getApplicationContext(), getString(R.string.notOldPass), Toast.LENGTH_LONG).show();
        } else
            Toast.makeText(getApplicationContext(), getString(R.string.passwordsEmpty), Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            if (edOldPass.hasFocus()) {
                if (!edOldPass.getText().toString().equals("")){
                    edOldPass.clearFocus();
                    edNewPass.requestFocus();
                    return true;
                }
                return false;
            } else if (edNewPass.hasFocus()) {
                if (!edNewPass.getText().toString().equals("")) {
                    edNewPass.clearFocus();
                    edRepPass.requestFocus();
                    return true;
                }
                return false;
            } else if (edRepPass.hasFocus()) {
                if (!edRepPass.getText().toString().equals("")) {
                    edRepPass.clearFocus();
                    getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
                    btnChange.performClick();
                    return true;
                }
                return false;
            }
        }
        return super.onKeyUp(keyCode, event);
    }
}