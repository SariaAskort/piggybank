package com.example.pmdm_t3a9_furio_andreu.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pmdm_t3a9_furio_andreu.R;
import com.example.pmdm_t3a9_furio_andreu.adapters.OneListItemAdapter;
import com.example.pmdm_t3a9_furio_andreu.bd.MiBancoOperacional;
import com.example.pmdm_t3a9_furio_andreu.pojo.Cliente;
import com.example.pmdm_t3a9_furio_andreu.pojo.Cuenta;
import com.example.pmdm_t3a9_furio_andreu.pojo.Movimiento;

import java.util.ArrayList;
import java.util.Date;

public class Transfer extends AppCompatActivity {

    private GridView gridBankAccounts;
    private Spinner myAccountSpinner;
    private GridView otherAccounts;
    private EditText OtherAccountInput;
    private CheckBox chkReceipt;
    private Button btnOkTransfer;
    private Button btnCancelTransfer;
    private TextView oldSelected = null;
    private TextView otherSelected = null;
    private Spinner CurrencySpinner;
    private EditText edQuantity;
    private Cliente cliente;
    private EditText txtDescTransfer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        gridBankAccounts = findViewById(R.id.gridBankAccounts);
        otherAccounts = findViewById(R.id.otherAccounts);
        myAccountSpinner = findViewById(R.id.myAccountsSpinner);
        OtherAccountInput = findViewById(R.id.OtherAccountInput);
        chkReceipt = findViewById(R.id.chkReceipt);
        btnOkTransfer = findViewById(R.id.btnOkTransfer);
        btnCancelTransfer = findViewById(R.id.btnCancelTransfer);
        CurrencySpinner = findViewById(R.id.CurrencySpinner);
        edQuantity = findViewById(R.id.edQuantity);
        txtDescTransfer = findViewById(R.id.txtDescTransfer);

        cliente = (Cliente) getIntent().getSerializableExtra("client");

        MiBancoOperacional mbo = MiBancoOperacional.getInstance(this);

        ArrayList<Cuenta> cuentas = mbo.getCuentas(cliente);

        ArrayList<String> myAccounts = new ArrayList<>();
        for (Cuenta c :
                cuentas) {
            myAccounts.add(c.getNumeroCuenta());
        }

        final OneListItemAdapter adapterGrid = new OneListItemAdapter(this, myAccounts);
        gridBankAccounts.setAdapter(adapterGrid);

        gridBankAccounts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (oldSelected != null)
                    oldSelected.setSelected(false);
                view.setSelected(true);
                oldSelected = (TextView) view;
            }
        });

        otherAccounts.setAdapter(adapterGrid);
        otherAccounts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (otherSelected != null)
                    otherSelected.setSelected(false);
                view.setSelected(true);
                otherSelected = (TextView) view;
            }
        });

        String[] currencies = {"€", "$", "£", "¥", "₽"};
        ArrayAdapter<String> adapterCurrency = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, currencies);
        adapterCurrency.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        CurrencySpinner.setAdapter(adapterCurrency);

    }

    public void ok(View v) {
        MiBancoOperacional mbo = MiBancoOperacional.getInstance(getApplicationContext());
        if (oldSelected != null) {
            if (otherSelected != null) {
                if (!oldSelected.getText().toString().equals(otherSelected.getText().toString())) {
                    if (!edQuantity.getText().toString().isEmpty()) {
                        if (!txtDescTransfer.getText().toString().isEmpty()){
                            Cuenta cuentaOrigen = getCuenta(oldSelected.getText().toString());
                            Cuenta cuentaDestino = getCuenta(otherSelected.getText().toString());
                            if (cuentaOrigen != null) {
//                            if (chkReceipt.isChecked()) {
//                            Toast.makeText(getApplicationContext(), oldSelected.getText().toString() + "\n" + otherAccountNumber + "\n" + edQuantity.getText().toString() + CurrencySpinner.getSelectedItem().toString() + "\n" + getString(R.string.Checked), Toast.LENGTH_LONG).show();
//                            } else {
//                            Toast.makeText(getApplicationContext(), oldSelected.getText().toString() + "\n" + otherAccountNumber + "\n" + edQuantity.getText().toString() + CurrencySpinner.getSelectedItem().toString() + "\n" + getString(R.string.NotChecked), Toast.LENGTH_LONG).show();
//                            }
                                Movimiento movimiento = new Movimiento(1, 0, new Date(), txtDescTransfer.getText().toString(), Float.parseFloat(edQuantity.getText().toString()), cuentaOrigen, cuentaDestino);
                                int resultado = mbo.transferencia(movimiento);
                                switch (resultado) {
                                    case 1:
                                        Toast.makeText(getApplicationContext(), getString(R.string.destinationAccountDoesntExist), Toast.LENGTH_LONG).show();
                                        break;

                                    case 2:
                                        Toast.makeText(getApplicationContext(), getString(R.string.notEnoughMoney), Toast.LENGTH_LONG).show();
                                        break;

                                    case 0:
                                        Toast.makeText(getApplicationContext(), getString(R.string.successfulOperation), Toast.LENGTH_LONG).show();
                                        break;
                                }
                            }
                        } else Toast.makeText(getApplicationContext(), getString(R.string.NoDescription), Toast.LENGTH_LONG).show();
                    } else Toast.makeText(getApplicationContext(), getString(R.string.NoQuantityInput), Toast.LENGTH_SHORT).show();
                } else Toast.makeText(getApplicationContext(), getString(R.string.sameAccount), Toast.LENGTH_SHORT).show();
            } else Toast.makeText(getApplicationContext(), getString(R.string.NoOtherAccountSelected), Toast.LENGTH_LONG).show();
        } else Toast.makeText(getApplicationContext(), getString(R.string.NoAccountSelected), Toast.LENGTH_LONG).show();
    }


    public void cancel(View v) {
        gridBankAccounts.setSelected(false);
        chkReceipt.setChecked(false);
        edQuantity.setText("");
        if (oldSelected != null) {
            oldSelected.setSelected(false);
            oldSelected = null;
        }
        if (otherSelected != null){
            otherSelected.setSelected(false);
            otherSelected = null;
        }
    }

    private Cuenta getCuenta(String numero) {
        ArrayList<Cuenta> cuentas = cliente.getListaCuentas();
        for (Cuenta cuenta :
                cuentas) {
            if (cuenta.getNumeroCuenta().equalsIgnoreCase(numero)) return cuenta;
        }
        return null;
    }

    @Override
    public void onBackPressed() {
        MiBancoOperacional mbo = MiBancoOperacional.getInstance(getApplicationContext());
        Cliente actualizado = mbo.login(cliente);
        actualizado = fillCliente(actualizado, mbo);
        Intent intent = new Intent(Transfer.this, Dashboard.class);
        intent.putExtra("client", actualizado);
        startActivity(intent);
    }

    public Cliente fillCliente(Cliente cliente, MiBancoOperacional mbo) {
        cliente.setListaCuentas(mbo.getCuentas(cliente));
        for (Cuenta cuenta :
                cliente.getListaCuentas()) {
            cuenta.setListaMovimientos(mbo.getMovimientos(cuenta));
        }
        return cliente;
    }
}