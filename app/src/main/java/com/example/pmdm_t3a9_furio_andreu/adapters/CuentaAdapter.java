package com.example.pmdm_t3a9_furio_andreu.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.pmdm_t3a9_furio_andreu.R;
import com.example.pmdm_t3a9_furio_andreu.pojo.Cuenta;

import java.util.ArrayList;

public class CuentaAdapter extends ArrayAdapter<Cuenta> {
    Activity context;
    ArrayList<Cuenta> cuentas;

    public CuentaAdapter(Fragment context, ArrayList<Cuenta> objects) {
        super(context.getActivity(), R.layout.account_list_item, objects);
        this.context = context.getActivity();
        this.cuentas = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.account_list_item, null);

        TextView accountNumber = item.findViewById(R.id.accountNumber);
        TextView bank = item.findViewById(R.id.bank);
        TextView balance = item.findViewById(R.id.balance);

        Cuenta cuenta = (Cuenta) getItem(position);
        accountNumber.setText(cuenta.getNumeroCuenta());
        bank.setText(cuenta.getBanco());
        double saldo = cuenta.getSaldoActual();
        balance.setText(String.valueOf(saldo));
        if (saldo >= 0) {
            balance.setTextColor(Color.GREEN);
        } else {
            balance.setTextColor(Color.RED);
        }

        return item;
    }
}
