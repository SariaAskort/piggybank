package com.example.pmdm_t3a9_furio_andreu.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.pmdm_t3a9_furio_andreu.R;
import com.example.pmdm_t3a9_furio_andreu.pojo.Movimiento;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MovementAdapter extends ArrayAdapter<Movimiento> {
    private Activity context;
    private ArrayList<Movimiento> data;

    public MovementAdapter(Fragment context, ArrayList<Movimiento> objects) {
        super(context.getActivity(), R.layout.movement_list_item, objects);
        this.context = context.getActivity();
        this.data = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listItemView = inflater.inflate(R.layout.movement_list_item, null);

        TextView txtDate = listItemView.findViewById(R.id.txtDate);
        TextView txtAmount = listItemView.findViewById(R.id.txtAmount);
        TextView txtDescription = listItemView.findViewById(R.id.txtDescription);

        Movimiento movimiento = (Movimiento) getItem(position);
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        txtDate.setText(formateador.format(movimiento.getFechaOperacion()));
        txtAmount.setText(String.valueOf(movimiento.getImporte()));
        if (movimiento.getImporte() < 0) {
            txtAmount.setTextColor(Color.RED);
        } else {
            txtAmount.setTextColor(Color.GREEN);
        }
        txtDescription.setText(movimiento.getDescripcion());
        return listItemView;
    }
}
