package com.example.pmdm_t3a9_furio_andreu.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.pmdm_t3a9_furio_andreu.R;

import java.util.ArrayList;
import java.util.List;

public class OneListItemAdapter extends ArrayAdapter<String> {
    Activity context;
    ArrayList<String> data;

    public OneListItemAdapter(Context context, List<String> objects) {
        super(context, 0, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View listItemView = convertView;

        if (null == convertView) {
            listItemView =inflater.inflate(R.layout.one_item_list, parent, false);
        }

        TextView txt = listItemView.findViewById(R.id.txtDato);
        String item = getItem(position);
        txt.setText(item);

        return listItemView;
    }
}
