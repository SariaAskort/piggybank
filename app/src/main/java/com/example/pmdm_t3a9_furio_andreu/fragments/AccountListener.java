package com.example.pmdm_t3a9_furio_andreu.fragments;

import com.example.pmdm_t3a9_furio_andreu.pojo.Cuenta;

public interface AccountListener {
    void onAccountSelected(Cuenta cuenta);
}
