package com.example.pmdm_t3a9_furio_andreu.fragments;

import android.accounts.Account;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.pmdm_t3a9_furio_andreu.R;
import com.example.pmdm_t3a9_furio_andreu.adapters.CuentaAdapter;
import com.example.pmdm_t3a9_furio_andreu.bd.MiBancoOperacional;
import com.example.pmdm_t3a9_furio_andreu.pojo.Cliente;
import com.example.pmdm_t3a9_furio_andreu.pojo.Cuenta;

import java.util.ArrayList;

public class FragmentCuentas extends Fragment {
    private ListView accountList;
    private ArrayList<Cuenta> cuentas;
    private AccountListener listener;

/*    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }*/

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_list, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //if (getArguments() != null) {
            cuentas = (ArrayList<Cuenta>) getArguments().getSerializable("cuentas");
        //}
        accountList = getView().findViewById(R.id.list);
        accountList.setAdapter(new CuentaAdapter(this, cuentas));

        accountList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listener != null) {
                    listener.onAccountSelected((Cuenta) accountList.getAdapter().getItem(position));
                }
            }
        });
    }

    public void setAccountArray(ArrayList<Cuenta> cuentas) {
        this.cuentas = cuentas;
    }

    public void setOnAccountSelected(AccountListener listener) {
        this.listener = listener;
    }
}
