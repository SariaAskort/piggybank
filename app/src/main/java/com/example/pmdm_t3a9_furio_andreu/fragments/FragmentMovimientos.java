package com.example.pmdm_t3a9_furio_andreu.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.pmdm_t3a9_furio_andreu.R;
import com.example.pmdm_t3a9_furio_andreu.adapters.MovementAdapter;
import com.example.pmdm_t3a9_furio_andreu.pojo.Movimiento;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class FragmentMovimientos extends Fragment {
    private View view;
    private ListView lista;
    private ArrayList<Movimiento> movimientos;

    public FragmentMovimientos() {
    }

    public static FragmentMovimientos newInstance(ArrayList<Movimiento> movimientos) {
        FragmentMovimientos f = new FragmentMovimientos();
        Bundle args = new Bundle();
        args.putSerializable("movimientos", movimientos);
        f.setArguments(args);
        return f;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.layout_list, container, false);
        movimientos = (ArrayList<Movimiento>) getArguments().getSerializable("movimientos");
        lista = view.findViewById(R.id.list);

        lista.setAdapter(new MovementAdapter(this, movimientos));

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Movimiento movimiento = (Movimiento) FragmentMovimientos.this.lista.getAdapter().getItem(position);

                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

                SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");

                dialog.setMessage(getString(R.string.id) + " " + movimiento.getId() + "\n" + getString(R.string.type) + " " + movimiento.getTipo() + "\n" + getString(R.string.operationDate) + " " + formateador.format(movimiento.getFechaOperacion())
                        + "\n" + getString(R.string.description) + " " + movimiento.getDescripcion() + "\n" + getString(R.string.cost) + " " + movimiento.getImporte() + "\n"
                        + getString(R.string.idOrigAccount) + " " + movimiento.getCuentaOrigen().getId() + "\n" + getString(R.string.idDestAccount) + " " + movimiento.getCuentaDestino().getId())
                        .setTitle(getString(R.string.movementInfo))
                        .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).create().show();
            }
        });
        return view;
    }

//    public void showMovements() {
//        view = getView().findViewById(R.id.list);
//
//        view.setAdapter(new MovementAdapter(this, movimientos));
//
//        view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Movimiento movimiento = (Movimiento) FragmentMovimientos.this.view.getAdapter().getItem(position);
//
//                AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
//
//                SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
//
//                dialog.setMessage(getString(R.string.id) + " " + movimiento.getId() + "\n" + getString(R.string.type) + " " + movimiento.getTipo() + "\n" + getString(R.string.operationDate) + " " + formateador.format(movimiento.getFechaOperacion())
//                        + "\n" + getString(R.string.description) + " " + movimiento.getDescripcion() + "\n" + getString(R.string.cost) + " " + movimiento.getImporte() + "\n"
//                        + getString(R.string.idOrigAccount) + " " + movimiento.getCuentaOrigen().getId() + "\n" + getString(R.string.idDestAccount) + " " + movimiento.getCuentaDestino().getId())
//                        .setTitle(getString(R.string.movementInfo))
//                        .setPositiveButton(getString(android.R.string.ok), new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.cancel();
//                            }
//                        }).create().show();
//            }
//        });
//    }
}
